package com.tkauk.chessapp;

/**
 * Created by tkauk on 4/20/2017.
 */
public class Rook extends Piece{
    boolean hasmoved=false;

    public Rook(String coord, char type, char color) {
        super(coord, type, color);
        // TODO Auto-generated constructor stub
    }


    public Rook(int row, int col, char type, char color){
        super(row,col,type,color);
    }

    public Rook(int row, int col, char type, char color, boolean hasmoved){
        super(row, col,type,color);
        this.hasmoved=hasmoved;
    }
    @Override
    public String toString(){
        if(color == 'w'){
            return "wR";
        }
        return "bR";

    }


    @Override
    public boolean validmovement(String origin, String dest, ChessBoard b) {
        // TODO Auto-generated method stub


        //moving across rows
/*
        int[] o = new int[2];
        int[] d = new int[2];

        o[0] = b.origin.getRow();
        o[1] = b.origin.getCol();

        d[0] = b.dest.getRow();
        d[1] = b.dest.getCol();
        */
        int[] o = b.getCoord(origin);
        int[] d = b.getCoord(dest);

        if(b.getSquare(d[0],d[1]).getPiece()!=null){
            if(b.getSquare(o[0],o[1]).getPiece().getColor()==b.getSquare(d[0],d[1]).getPiece().getColor()){
                return false;
            }
        }

        //rook is trying to move diagonally
        if(o[0] != d[0] && o[1] != d[1]){
            //System.out.println("Error rooks cant move diagonally!");
            return false;

        }

        //checking if there is something in its path
        //moving across columns
        if(o[0]== d[0]){

            if(o[1] <d[1]){
                for(int i=1;i<Math.abs(o[1]-d[1]);i++){
                    if(b.getSquare(o[0], o[1]+i).getPiece() != null){
                        return false;
                    }
                }
            }else if(o[1]>d[1]){
                for(int i=1;i<Math.abs(o[1]-d[1]);i++){
                    if(b.getSquare(o[0], o[1]-i).getPiece() != null){
                        return false;
                    }
                }
            }}
        //moving across rows
        if(o[1] == d[1]){

            if(o[0]<d[0]){
                for(int i=1;i<Math.abs(o[0]-d[0]);i++){
                    if(b.getSquare(o[0]+i, o[1]).getPiece() != null){
                        return false;
                    }
                }
            }else if(o[0]>d[0]){
                for(int i=1;i<Math.abs(o[0]-d[0]);i++){
                    if(b.getSquare(o[0]-i, o[1]).getPiece() != null){
                        return false;
                    }
                }
            }



        }

        //moving across rows



        hasmoved=true;
        return true;

    }


}

