package com.tkauk.chessapp;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by tkauk on 4/28/2017.
 */

public class list_games extends AppCompatActivity {
    private ListView lv;
    private GameData gameData;
    public Game item;

   // RelativeLayout rel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.chessboard_layout);
        setContentView(R.layout.playback);
        try {
            FileInputStream fis = openFileInput("Games.ser");
            ObjectInputStream ois = new ObjectInputStream(fis);
            this.gameData = (GameData) ois.readObject();
            fis.close();
            ois.close();
        } catch (Exception e){
            Log.v("Serialization Error : ",e.getMessage());
            e.printStackTrace();
        }

        if(gameData==null){
            gameData=new GameData();
        }
        final ArrayAdapter<Game> arrayAdapter = new ArrayAdapter<Game>(
                this,
                android.R.layout.simple_list_item_1,
                gameData.getGames() );


       final  ListView lv = (ListView) findViewById(R.id.games);
        lv.setAdapter(arrayAdapter);


        lv.setOnItemClickListener(new OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position, long l){
                item = (Game) adapter.getItemAtPosition(position);
                for (int i = 0; i < lv.getChildCount(); i++) {
                    if(position == i ){
                        lv.getChildAt(i).setBackgroundColor(Color.BLUE);
                    }else{
                        lv.getChildAt(i).setBackgroundColor(Color.TRANSPARENT);
                    }
                }
                /*
                Intent intent = new Intent(v.getContext(),Replay.class);
                intent.putExtra("GAME", item.getName());
                //based on item add info to intent
                startActivity(intent);
                */
            }
        });




        Button sortbyname = (Button) findViewById(R.id.sort_by_name);
        sortbyname.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                gameData.listbyname();
               /* try {
                    GameData.write(gameData, view.getContext());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    gameData = GameData.read(view.getContext());
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                */
                lv.setAdapter(arrayAdapter);

            }

});
        Button sortbydate = (Button) findViewById(R.id.sort_by_date);
        sortbydate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                gameData.listbydate();
                lv.setAdapter(arrayAdapter);

            }

        });
        Button quit = (Button) findViewById(R.id.list_quit);
        quit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                try {
                    FileOutputStream fileOutputStream = openFileOutput("Game.ser", Context.MODE_PRIVATE);
                    ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
                    objectOutputStream.writeObject(gameData);
                    objectOutputStream.flush();
                    objectOutputStream.close();
                    fileOutputStream.close();
                }catch(IOException e){
                    Log.v("Serialization Error : ",e.getMessage());
                    e.printStackTrace();
                }
                Intent myIntent = new Intent(view.getContext(), MainActivity.class);
                startActivityForResult(myIntent, 0);
                finish();
            }

        });
        Button listdelete = (Button) findViewById(R.id.list_delete);
        listdelete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Game g = item;
                if(g==null){
                    return;
                }
                gameData.removeGame(g);
                lv.setAdapter(arrayAdapter);
                try {
                    GameData.write(gameData, view.getContext());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        });
        Button listreplay = (Button) findViewById(R.id.list_replay);
        listreplay.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Game g = item;
                if(g==null){
                    return;
                }
                Intent intent = new Intent(view.getContext(),Replay.class);
                intent.putExtra("GAME", g.getName());
                startActivity(intent);
                finish();
            }

        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
