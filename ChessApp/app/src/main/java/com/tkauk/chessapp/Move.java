package com.tkauk.chessapp;

import java.io.Serializable;

/**
 * Created by tkauk on 4/26/2017.
 */

public class Move implements Serializable{

    private String origin;
    private String dest;
    private char promote = 'X';
    private boolean canUndo;


    public Move(String o, String d){
        this.origin = o;
        this.dest = d;
        this.promote = 'X';
        this.canUndo=true;

    }

    public char getPromote() {
        return promote;
    }

    public void setPromote(char promote) {
        this.promote = promote;
    }

    public String getOrigin() {

        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDest() {

        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public boolean isCanUndo() {
        return canUndo;
    }

    public void setCanUndo(boolean canUndo) {
        this.canUndo = canUndo;
    }

    public Move(String o, String d, char p){
        this.origin = o;
        this.dest = d;
        this.promote = p;
        this.canUndo = true;



    }

    public Move(Move another){
        this.origin=another.getOrigin();
        this.dest=another.getDest();

        this.promote=another.getPromote();
        this.canUndo=another.isCanUndo();
    }





}
