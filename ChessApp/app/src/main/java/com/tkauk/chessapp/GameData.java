package com.tkauk.chessapp;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by zeyuh on 2017/4/23.
 */

public class GameData implements Serializable {
    private static final long serialVersionUID = 3L;
   // public static final String storeDir = "src";
    public static final String storeFile = "Games.ser";
    public ArrayList<Game> Games;

    public GameData(){
        this.Games=new ArrayList<Game>();
    }
    public ArrayList<Game> getGames() {
        return Games;
    }

    public void setGames(ArrayList<Game> Games) {
        this.Games = Games;
    }

    public void AddGame(Game g){

        this.Games.add(g);
    }

    public void removeGame(Game g){

        this.Games.remove(g);
    }
    public void listbyname(){

        Collections.sort(Games,Game.GameNameComparator);
    }

    public boolean contain(String gname){
        for(int i=0;i<Games.size()-1;i++){
            if(gname.equals(Games.get(i).getName())){
                return true;
            }
        }
        return false;
    }
    public void listbydate(){
        Collections.sort(Games,Game.GameDateComparator);
    }

    public static GameData read(Context context) throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = context.openFileInput(storeFile);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        GameData games = (GameData) objectInputStream.readObject();
        objectInputStream.close();
        fileInputStream.close();

        return games;
    }

    public static void write(GameData Gamelist, Context context) throws IOException {
        FileOutputStream fileOutputStream = context.openFileOutput(storeFile, Context.MODE_PRIVATE);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(Gamelist);
        objectOutputStream.close();
        fileOutputStream.close();
    }
}
