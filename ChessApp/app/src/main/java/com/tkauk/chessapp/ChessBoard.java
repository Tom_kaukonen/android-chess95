package com.tkauk.chessapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.NoSuchElementException;

import static android.R.attr.id;

/**
 * Created by tkauk on 4/20/2017.
 */

public class ChessBoard extends View implements Serializable {
    public Square[][] chessboard;
    //must be defined here
    //overwrite the onDraw method
    public int num_selected = 0;
    private int x0 = 0;
    private int y0 = 0;
    private int squareSize = 0;
    Square origin = null;
    Square dest = null;
    public static int movecount = 0;
    boolean wmove = true;
    public ChessBoard prev;
    public char promote = 'X';
    public boolean promoting;
    public ArrayList<Move> moves;


    public ChessBoard(Context context) {
        super(context);
        this.chessboard = new Square[8][8];
        this.moves = new ArrayList<Move>();
        setSquares();
        setBlack();
        setWhite();
        this.prev = null;
        this.promoting = false;
        //this.origin.Selected1 = true;
        this.origin = null;
        this.dest=null;
        this.movecount=0;


    }

    public ChessBoard(Context context, ChessBoard another) {
        super(another.getContext());


        chessboard = new Square[8][8];
        setSquares();
        for (int i = 0; i < another.chessboard.length; i++) {
            for (int j = 0; j < another.chessboard.length; j++) {
                if (!another.getSquare(i, j).empty) {
                    char color = another.getSquare(i, j).getPiece().getColor();
                    char type = another.getSquare(i, j).getPiece().getType();
                    if (type == 'P') {
                        Pawn r = (Pawn) another.getSquare(i, j).getPiece();
                        boolean hasMoved = r.hasMoved;
                        boolean movedtwoSquares = r.movedtwoSquares;
                        int move = r.move;
                       // Toast.makeText(this.getContext(), "movecount: " + r.move, Toast.LENGTH_SHORT).show();
                        chessboard[i][j].setPiece(new Pawn(i, j, 'P', color, hasMoved, movedtwoSquares, move));
                    } else if (type == 'K') {
                        King r = (King) another.getSquare(i, j).getPiece();
                        boolean hasMoved = r.hasmoved;
                        chessboard[i][j].setPiece(new King(i, j, 'K', color, hasMoved));
                    } else if (type == 'N') {
                        chessboard[i][j].setPiece(new Knight(i, j, 'N', color));
                    } else if (type == 'Q') {
                        chessboard[i][j].setPiece(new Queen(i, j, 'Q', color));
                    } else if (type == 'R') {
                        Rook r = (Rook) another.getSquare(i, j).getPiece();
                        boolean hasmoved = r.hasmoved;
                        chessboard[i][j].setPiece(new Rook(i, j, 'R', color, hasmoved));
                    } else if (type == 'B') {
                        chessboard[i][j].setPiece(new Bishop(i, j, 'B', color));
                    }
                }
            }
        }
    }


    private void setSquares() {
        for (int r = 0; r < 8; r++) {
            for (int c = 0; c < 8; c++) {
                chessboard[r][c] = new Square(r, c);
            }
        }
    }


    private void setWhite() {
        //chessboard[1][2].setPiece(new Pawn(1,2,'P', 'w'));
        //chessboard[1][4].setPiece(new King(1,5,'K','w'));
        //chessboard[7][5].setPiece(new Queen(6,6,'Q','w'));
        //chessboard[7][0].setPiece(new King(7,0,'K','w'));
        for (int i = 0; i < chessboard.length; i++) {
            for (int j = 0; j < chessboard.length; j++) {
                //its a pawn
                if (i == 6) {

                    //	System.out.println("creating pawns");
                   chessboard[i][j].setPiece(new Pawn(i, j, 'P', 'w'));
                }

                if ((i == 7 && j == 0) || (i == 7 && j == 7)) {

                    chessboard[i][j].setPiece(new Rook(i, j, 'R', 'w'));
                }

                if ((i == 7 && j == 1) || (i == 7 && j == 6)) {

                    chessboard[i][j].setPiece(new Knight(i, j, 'N', 'w'));
                }

                if ((i == 7 && j == 2) || (i == 7 && j == 5)) {
                    chessboard[i][j].setPiece(new Bishop(i, j, 'B', 'w'));
                }


                if ((i == 7 && j == 3)) {

                    chessboard[i][j].setPiece(new Queen(i, j, 'Q', 'w'));
                }

                if ((i == 7 && j == 4)) {

                    chessboard[i][j].setPiece(new King(i, j, 'K', 'w'));
                }


            }
        }

    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int width = getWidth();
        int height = getHeight();
        this.squareSize = Math.min(
                (width / 8),
                (height / 8)
        );


        for (int r = 0; r < 8; r++) {
            for (int c = 0; c < 8; c++) {
                int xCoord = getXCoord(c);
                int yCoord = getYCoord(r);

                Rect tileRect = new Rect(
                        xCoord,               // left
                        yCoord,               // top
                        xCoord + squareSize,  // right
                        yCoord + squareSize   // bottom
                );
                chessboard[r][c].setTileRect(tileRect);
                String type = chessboard[r][c].toString();
                chessboard[r][c].draw(canvas, type, getContext());

            }
        }
    }


    private void setBlack() {
        //chessboard[6][3].setPiece(new Queen(6,3,'Q','b'));
        //chessboard[0][0].setPiece(new King(0,0,'K','b'));
        for (int i = 0; i < chessboard.length; i++) {
            for (int j = 0; j < chessboard.length; j++) {
                //its a pawn

                if (i == 1) {
                    chessboard[i][j].setPiece(new Pawn(i, j, 'P', 'b'));
                }

                if ((i == 0 && j == 0) || (i == 0 && j == 7)) {

                    chessboard[i][j].setPiece(new Rook(i, j, 'R', 'b'));
                }

                if ((i == 0 && j == 1) || (i == 0 && j == 6)) {

                    chessboard[i][j].setPiece(new Knight(i, j, 'N', 'b'));
                }

                if ((i == 0 && j == 2) || (i == 0 && j == 5)) {

                     chessboard[i][j].setPiece(new Bishop(i, j, 'B', 'b'));
                }


                if ((i == 0 && j == 3)) {

                   chessboard[i][j].setPiece(new Queen(i, j, 'Q', 'b'));
                }

                if ((i == 0 && j == 4)) {

                    chessboard[i][j].setPiece(new King(i, j, 'K', 'b'));
                }


            }

        }


    }


    public Square[][] getChessboard() {
        return chessboard;
    }

    public void setChessboard(Square[][] chessboard) {
        this.chessboard = chessboard;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getActionMasked();
        Square s;

           /* switch (action) {

            case MotionEvent.ACTION_DOWN:*/

                final int initx = (int) event.getX();
                final int inity = (int) event.getY();
                for (int c = 0; c < 8; c++) {
                    for (int r = 0; r < 8; r++) {
                        s = chessboard[c][r];
                        if (s.isTouched(initx, inity)) {

                            if (s.Selected1 && origin == null && !s.empty && s.rightmove(this)) {
                                s.Selected1 = false;
                                // s.setPiece(new Queen(r, c, 'Q', 'w'));

                                origin = chessboard[c][r];
                                //Paint(tem) = new Paint();
                            }

                            if (s.Selected1 && origin != null) {

                                //s.Selected1 = false;
                                dest = chessboard[c][r];
                                final Piece p = origin.p;
                                int orgR = origin.getCol();
                                int orgC = origin.getRow();
                                Pawn pe = null;
                                if(p.getType() == 'P') {
                                     pe = new Pawn(p.getRow(), p.getCol(), p.getType(), p.getColor());
                                }
                                int[] o = this.getCoord(Piece.genCoord(orgR, orgC));
                                int[] d = this.getCoord(Piece.genCoord(r, c));
                                String org = Piece.genCoord(orgR, orgC);
                                //Toast.makeText(this.getContext(),org,Toast.LENGTH_SHORT).show();
                                // Toast.makeText(this.getContext(),org,Toast.LENGTH_SHORT).show();
                                // String des = "row: " + d[0] + ", col: " + d[1];
                                //Toast.makeText(this.getContext(),des,Toast.LENGTH_SHORT).show();
                                ChessBoard ttemp = new ChessBoard(getContext(), this);
                                prev = new ChessBoard(this.getContext(), this);
                                ChessBoard temp = new ChessBoard(this.getContext(), this);

                                //promotion case


                                if (pe != null && pe.validmovement(Piece.genCoord(orgR, orgC), Piece.genCoord(r, c), temp) && (Math.abs(dest.getRow() - origin.getRow()) == 1) && (dest.getRow() == 0 || dest.getRow() == 7)) {
                                   // Toast.makeText(this.getContext(), "promoting", Toast.LENGTH_SHORT).show();


                                    ttemp.move(Piece.genCoord(orgR, orgC), Piece.genCoord(r, c), ttemp);
                                    if (ttemp.checkcheck(!wmove)) {
                                        Toast.makeText(this.getContext(), "The move will make you King into check", Toast.LENGTH_SHORT).show();
                                        origin.setPiece(p);
                                        p.setRow(r);
                                        p.setCol(c);

                                    }

                                    else if (pe.getColor() == 'w' && dest.getRow() == 0) {


                                        this.promoting = true;
                                        Promote(this);
                                        return super.onTouchEvent(event);
                                    }


                                    else if (pe.getColor() == 'b' && dest.getRow() == 7) {


                                        Promote(this);
                                        this.promoting = true;
                                        return super.onTouchEvent(event);
                                    }


                                }


                                 else if (p.validmovement(Piece.genCoord(orgR, orgC), Piece.genCoord(r, c), this) && !(p.getType() == 'P' && (dest.getRow() == 0 || dest.getRow() == 7))) {

                                    //Toast.makeText(this.getContext(), "still promoting", Toast.LENGTH_SHORT).show();


                                    //Toast.makeText(this.getContext(), "testing pawn", Toast.LENGTH_SHORT).show();

                                    ttemp.move(Piece.genCoord(orgR, orgC), Piece.genCoord(r, c), ttemp);
                                    if (ttemp.checkcheck(!wmove)) {
                                        Toast.makeText(this.getContext(), "The move will make you King into check", Toast.LENGTH_SHORT).show();
                                        origin.setPiece(p);
                                        p.setRow(r);
                                        p.setCol(c);

                                    } else {

                                       // Toast.makeText(this.getContext(), "lol", Toast.LENGTH_SHORT).show();



                                        straightmove(Piece.genCoord(orgR, orgC), Piece.genCoord(r, c));
                                        this.moves.add(new Move(Piece.genCoord(orgR, orgC), Piece.genCoord(r, c)));

                                        this.movecount++;
                                        if (this.checkcheck(wmove)) {


                                            if (this.checkmatecheck(wmove)) {
                                                Toast.makeText(this.getContext(), "Checkmate", Toast.LENGTH_SHORT).show();








                                                String result;
                                                if(wmove){
                                                    result="Checkmate,White Wins";
                                                }else{
                                                    result="Checkmate, Black Wins";
                                                }
                                                endgame(this,result);

                                            } else {
                                                Toast.makeText(this.getContext(), "Check", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                        if (this.wmove) {
                                            wmove = false;
                                        } else {
                                            wmove = true;
                                        }
                                    }
                                    this.origin.Selected1 = true;
                                    this.origin = null;
                                } else {

                                    origin.setPiece(p);
                                    p.setRow(r);
                                    p.setCol(c);
                                    origin.Selected1 = true;
                                    origin = null;
                                }


                            }


                        }
                        // Paint temp = new Paint();
                        //  temp.setColor(Color.parseColor("#00aa00"));
                        //chessboard[c][r].setColor(temp);
                        //  s.setColor(temp);
                        //chessboard[c][r].handleTouch();

                    }
                }
               // break;


       // }

        invalidate();
        return super.onTouchEvent(event);
    }

    public Square getSquare(int row, int col) {
        return chessboard[row][col];
    }

    public Square[][] getchessboard() {
        return this.chessboard;
    }


    public int[] getCoord(String location) {
        int[] ret = new int[2];
        char letter = location.charAt(0);
        char row = location.charAt(1);

        int col = letter - 97;

        int r = Math.abs(row - 56);

        ret[0] = r;
        ret[1] = col;

        return ret;


    }


    public void undo() {
        if (!this.moves.get(this.moves.size() - 1).isCanUndo()) {
            return;
        }


      /* if(!this.moves.get(this.moves.size()-2).isCanUndo()){
            return;}*/
        Toast.makeText(this.getContext(), "you pressed undo", Toast.LENGTH_SHORT).show();
        // Revert to the previous state.
        this.moves.remove(this.moves.size() - 1);
        this.moves.get(this.moves.size() - 1).setCanUndo(false);
        // renderPrev();

    }


    public void renderPrev(ChessBoard another) {

        //ArrayList<Move> moveL = this.moves;
        //this. = new ChessBoard(this.getContext());
        //TO DO:  allow pawns to capture enpassant;


        chessboard = new Square[8][8];
        setSquares();
        for (int i = 0; i < another.chessboard.length; i++) {
            for (int j = 0; j < another.chessboard.length; j++) {
                if (!another.getSquare(i, j).empty) {
                    char color = another.getSquare(i, j).getPiece().getColor();
                    char type = another.getSquare(i, j).getPiece().getType();
                    if (type == 'P') {
                        Pawn r = (Pawn) another.getSquare(i, j).getPiece();
                        boolean hasMoved = r.hasMoved;
                        boolean movedtwoSquares = r.movedtwoSquares;
                        int move = r.move;
                        chessboard[i][j].setPiece(new Pawn(i, j, 'P', color, hasMoved, movedtwoSquares, move));
                    } else if (type == 'K') {
                        King r = (King) another.getSquare(i, j).getPiece();
                        boolean hasMoved = r.hasmoved;
                        chessboard[i][j].setPiece(new King(i, j, 'K', color, hasMoved));
                    } else if (type == 'N') {
                        chessboard[i][j].setPiece(new Knight(i, j, 'N', color));
                    } else if (type == 'Q') {
                        chessboard[i][j].setPiece(new Queen(i, j, 'Q', color));
                    } else if (type == 'R') {
                        Rook r = (Rook) another.getSquare(i, j).getPiece();
                        boolean hasmoved = r.hasmoved;
                        chessboard[i][j].setPiece(new Rook(i, j, 'R', color, hasmoved));
                    } else if (type == 'B') {
                        chessboard[i][j].setPiece(new Bishop(i, j, 'B', color));
                    }
                }
            }
        }

        this.wmove = !wmove;
        this.movecount--;
        this.moves.remove(this.moves.size() - 1);
        invalidate();
        this.prev = null;

    }

    public void resigncv(){
        chessboard=new Square[8][8];
        setSquares();
        setBlack();
        setWhite();
        this.movecount=0;
        this.wmove=true;
        this.prev=null;
        for(int i=moves.size()-1; i>=0;i--){
            moves.remove(i);
        }
        this.promoting=false;
        this.promote='X';
        origin=null;
        invalidate();

        //dest=null;
       // this.origin.Selected1 = true;


    }
    private int getXCoord(int x) {
        return x0 + squareSize * x;
    }

    private int getYCoord(int y) {
        return y0 + squareSize * y;
    }

    public void straightmove(String origin, String dest) {
        int[] o = this.getCoord(origin);
        int[] d = this.getCoord(dest);

        if (this.getchessboard()[o[0]][o[1]].empty) {
            return;
        }
        Piece p = this.getchessboard()[o[0]][o[1]].getPiece();


        if (p.type == 'P' && (d[0] == 0 || d[0] == 7)) {
            //System.out.println("trying to promote");


            if (this.movecount % 2 == 0) {
                switch (promote) {
                    case 'X':
                        this.getchessboard()[d[0]][d[1]].setPiece(new Queen(d[0], d[1], 'Q', 'w'));
                        this.getchessboard()[o[0]][o[1]].rem();
                        break;

                    case 'Q':
                        this.getchessboard()[d[0]][d[1]].setPiece(new Queen(d[0], d[1], 'Q', 'w'));
                        this.getchessboard()[o[0]][o[1]].rem();
                        break;

                    case 'N':
                        this.getchessboard()[d[0]][d[1]].setPiece(new Knight(d[0], d[1], 'N', 'w'));
                        this.getchessboard()[o[0]][o[1]].rem();
                        break;

                    case 'R':
                        this.getchessboard()[d[0]][d[1]].setPiece(new Rook(d[0], d[1], 'R', 'w'));
                        this.getchessboard()[o[0]][o[1]].rem();
                        break;

                    case 'B':
                        this.getchessboard()[d[0]][d[1]].setPiece(new Bishop(d[0], d[1], 'B', 'w'));
                        this.getchessboard()[o[0]][o[1]].rem();
                        break;
                }

            }

            if (this.movecount % 2 == 1) {
                switch (promote) {
                    case 'X':
                        this.getchessboard()[d[0]][d[1]].setPiece(new Queen(d[0], d[1], 'Q', 'b'));
                        this.getchessboard()[o[0]][o[1]].rem();
                        break;

                    case 'Q':
                        this.getchessboard()[d[0]][d[1]].setPiece(new Queen(d[0], d[1], 'Q', 'b'));
                        this.getchessboard()[o[0]][o[1]].rem();
                        break;

                    case 'N':
                        this.getchessboard()[d[0]][d[1]].setPiece(new Knight(d[0], d[1], 'N', 'b'));
                        this.getchessboard()[o[0]][o[1]].rem();
                        break;

                    case 'R':
                        this.getchessboard()[d[0]][d[1]].setPiece(new Rook(d[0], d[1], 'R', 'b'));
                        this.getchessboard()[o[0]][o[1]].rem();
                        break;

                    case 'B':
                        this.getchessboard()[d[0]][d[1]].setPiece(new Bishop(d[0], d[1], 'B', 'b'));
                        this.getchessboard()[o[0]][o[1]].rem();
                        break;
                }

            }


        } else {

            this.getchessboard()[d[0]][d[1]].setPiece(p);
            chessboard[d[0]][d[1]].getPiece().setRow(d[0]);
            chessboard[d[0]][d[1]].getPiece().setCol(d[1]);
            this.getchessboard()[o[0]][o[1]].rem();
            //this.origin.Selected1 = true;
            //this.origin = null;
        }
    }

    public void Promote(final ChessBoard cv) {


        final Dialog myDialog = new Dialog(cv.getContext());
        View view = LayoutInflater.from(cv.getContext()).inflate(R.layout.promotion, null);
        myDialog.setContentView(view);
        myDialog.setCancelable(true);

        boolean isQueen = false;
        boolean isKnight = false;
        myDialog.show();
        final Button knight = (Button) myDialog.findViewById(R.id.knight);
        final Button rook = (Button) myDialog.findViewById(R.id.rook);
        final Button Bishop = (Button) myDialog.findViewById(R.id.bishop);
        final Button queen = (Button) myDialog.findViewById(R.id.queen);
        queen.setSelected(false);
        //Button no = (Button) myDialog.findViewById(R.id.no);
        queen.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                //Do your code here
                cv.promote = 'Q';
                // queen.setSelected(true);
                cv.straightmove(Piece.genCoord(cv.origin.getCol(), cv.origin.getRow()), Piece.genCoord(cv.dest.getCol(), cv.dest.getRow()));

                cv.moves.add(new Move(Piece.genCoord(cv.origin.getCol(), cv.origin.getRow()), Piece.genCoord(cv.dest.getCol(), cv.dest.getRow()), 'Q'));


                cv.movecount++;
                if (cv.checkcheck(cv.wmove)) {


                    if (cv.checkmatecheck(cv.wmove)) {
                        Toast.makeText(cv.getContext(), "Checkmate", Toast.LENGTH_SHORT).show();


                        String result = null;
                        if (cv.wmove) {

                            result = "Checkmate,Whit wins";

                        } else {

                            result = "Checkmate,Black wins";

                        }

                       endgame(cv,result);

                    } else {
                        Toast.makeText(cv.getContext(), "Check", Toast.LENGTH_SHORT).show();
                    }
                }
                if (cv.wmove) {
                    cv.wmove = false;
                } else {
                    cv.wmove = true;
                }


                cv.origin.Selected1 = true;
                cv.origin = null;
                cv.invalidate();
                myDialog.cancel();
            }
        });

        rook.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                //Do your code here
                cv.promote = 'R';
                // queen.setSelected(true);
                cv.straightmove(Piece.genCoord(cv.origin.getCol(), cv.origin.getRow()), Piece.genCoord(cv.dest.getCol(), cv.dest.getRow()));
                cv.moves.add(new Move(Piece.genCoord(cv.origin.getCol(), cv.origin.getRow()), Piece.genCoord(cv.dest.getCol(), cv.dest.getRow()), 'R'));

                cv.movecount++;
                if (cv.checkcheck(cv.wmove)) {


                    if (cv.checkmatecheck(cv.wmove)) {
                        Toast.makeText(cv.getContext(), "Checkmate", Toast.LENGTH_SHORT).show();


                        String result = null;
                        if (cv.wmove) {

                            result = "Checkmate,Whit wins";

                        } else {

                            result = "Checkmate,Black wins";

                        }

                        endgame(cv,result);

                    } else {
                        Toast.makeText(cv.getContext(), "Check", Toast.LENGTH_SHORT).show();
                    }
                }
                if (cv.wmove) {
                    cv.wmove = false;
                } else {
                    cv.wmove = true;
                }


                cv.origin.Selected1 = true;
                cv.origin = null;
                cv.invalidate();
                myDialog.cancel();
            }
        });


        knight.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                //Do your code here
                cv.promote = 'N';
                // queen.setSelected(true);
                cv.straightmove(Piece.genCoord(cv.origin.getCol(), cv.origin.getRow()), Piece.genCoord(cv.dest.getCol(), cv.dest.getRow()));
                cv.moves.add(new Move(Piece.genCoord(cv.origin.getCol(), cv.origin.getRow()), Piece.genCoord(cv.dest.getCol(), cv.dest.getRow()), 'N'));

                cv.movecount++;
                if (cv.checkcheck(cv.wmove)) {


                    if (cv.checkmatecheck(cv.wmove)) {
                        Toast.makeText(cv.getContext(), "Checkmate", Toast.LENGTH_SHORT).show();


                        String result = null;
                        if (cv.wmove) {

                            result = "Checkmate,Whit wins";

                        } else {

                            result = "Checkmate,Black wins";

                        }

                       endgame(cv,result);
                    } else {
                        Toast.makeText(cv.getContext(), "Check", Toast.LENGTH_SHORT).show();
                    }
                }
                if (cv.wmove) {
                    cv.wmove = false;
                } else {
                    cv.wmove = true;
                }


                cv.origin.Selected1 = true;
                cv.origin = null;
                cv.invalidate();
                myDialog.cancel();
            }
        });


        Bishop.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                //Do your code here
                cv.promote = 'B';
                // queen.setSelected(true);
                cv.straightmove(Piece.genCoord(cv.origin.getCol(), cv.origin.getRow()), Piece.genCoord(cv.dest.getCol(), cv.dest.getRow()));
                cv.moves.add(new Move(Piece.genCoord(cv.origin.getCol(), cv.origin.getRow()), Piece.genCoord(cv.dest.getCol(), cv.dest.getRow()),'B'));

                cv.movecount++;
                if (cv.checkcheck(cv.wmove)) {


                    if (cv.checkmatecheck(cv.wmove)) {
                        Toast.makeText(cv.getContext(), "Checkmate", Toast.LENGTH_SHORT).show();


                        String result = null;
                        if (cv.wmove) {

                            result = "Checkmate,Whit wins";

                        } else {

                            result = "Checkmate,Black wins";

                        }

                       endgame(cv, result);
                    } else {
                        Toast.makeText(cv.getContext(), "Check", Toast.LENGTH_SHORT).show();
                    }
                }
                if (cv.wmove) {
                    cv.wmove = false;
                } else {
                    cv.wmove = true;
                }


                cv.origin.Selected1 = true;
                cv.origin = null;
                cv.invalidate();
                myDialog.cancel();
            }
        });


        myDialog.show();


    }


    public String getKinglocation(boolean wmove) {
        String opponentKingdest = null;
        if (wmove) {
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    if (!this.getchessboard()[i][j].empty) {
                        if (this.getchessboard()[i][j].getPiece().getColor() == 'b' && this.getchessboard()[i][j].getPiece().getType() == 'K') {
                            int[] a = {i, j};
                            opponentKingdest = this.getlocation(a);
                            //System.out.println(opponentKingdest);
                        }
                    }
                }
            }
        } else {
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    if (!this.getchessboard()[i][j].empty) {
                        if (this.getchessboard()[i][j].getPiece().getColor() == 'w' && this.getchessboard()[i][j].getPiece().getType() == 'K') {
                            int[] a = {i, j};
                            opponentKingdest = this.getlocation(a);
                            //System.out.println(opponentKingdest);
                        }
                    }
                }
            }
        }
        //System.out.println(opponentKingdest);
        //the Null point should never reach
        return opponentKingdest;
    }

    public String getlocation(int[] Coord) {
        String location = null;
        if (Coord.length == 2) {
            int r = Coord[0];
            int col = Coord[1];
            char row = (char) ('0' + Math.abs(8 - r));
            char letter = (char) (col + 97);
            location = letter + "" + row;
        }
        return location;
    }


    public void AutomaticMove() {
        ArrayList<Move> moves = new ArrayList<Move>();
        ChessBoard temp = new ChessBoard(this.getContext(), this);
        prev = new ChessBoard(this.getContext(), this);
        if (wmove) {
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    //its whites move so look for a white piece
                    if (!getchessboard()[i][j].empty) {

                        if (getChessboard()[i][j].getPiece().getColor() == 'w') {
                            //Toast.makeText(this.getContext(), "yupppppp", Toast.LENGTH_SHORT).show();
                            String org = Piece.genCoord(this.chessboard[i][j].getCol(), this.chessboard[i][j].getRow());
                            Piece p = temp.chessboard[i][j].getPiece();

                            for (int r = 0; r < this.chessboard.length; r++) {
                                for (int c = 0; c < this.chessboard.length; c++) {
                                    String dest = Piece.genCoord(c, r);

                                    if (p.validmovement(org, dest, temp)) {
                                        ChessBoard ic = new ChessBoard(this.getContext(), this);
                                        move(org, dest, ic);
                                        if (ic.checkcheck(!wmove)) {
                                            break;
                                        }

                                        moves.add(new Move(org, dest));

                                    }


                                }


                            }

                        }
                    }

                }

            }
        } else {
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    //its whites move so look for a white piece
                    if (!getchessboard()[i][j].empty) {

                        if (getChessboard()[i][j].getPiece().getColor() == 'b') {
                            //Toast.makeText(this.getContext(), "yupppppp", Toast.LENGTH_SHORT).show();
                            String org = Piece.genCoord(this.chessboard[i][j].getCol(), this.chessboard[i][j].getRow());
                            Piece p = temp.chessboard[i][j].getPiece();

                            for (int r = 0; r < this.chessboard.length; r++) {
                                for (int c = 0; c < this.chessboard.length; c++) {
                                    String dest = Piece.genCoord(c, r);
                                    if (p.validmovement(org, dest, temp)) {
                                        ChessBoard ic = new ChessBoard(this.getContext(), this);

                                       // Toast.makeText(this.getContext(), "ze king can move", Toast.LENGTH_SHORT).show();
                                        move(org, dest, ic);

                                        if (ic.checkcheck(!wmove)) {
                                            break;
                                        }



                                        moves.add(new Move(org, dest));

                                    }


                                }


                            }

                        }
                    }

                }

            }

        }


        // moves.add(new Move("e2", "e4"));
        if (moves.isEmpty()) {
            throw new NoSuchElementException("No moves.");
        }

        Move m = moves.get((int) (Math.random() * moves.size()));
        // TODO possible castle, possible pawn promotion
        //Toast.makeText(this.getContext(), m.getOrigin(), Toast.LENGTH_SHORT).show();


        this.moves.add(m);
        straightmove(m.getOrigin(), m.getDest());
        this.movecount++;
        // move("e2","e4",this);
        //this.move.add(Piece.genCoord(orgR, orgC) + "" + Piece.genCoord(r, c));

        //this.wmove = !wmove;
        // origin.Selected1 = true;
        //origin = null;
        if (this.checkcheck(wmove)) {


            if (this.checkmatecheck(wmove)) {
                Toast.makeText(this.getContext(), "Checkmate", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this.getContext(), "Check", Toast.LENGTH_SHORT).show();
            }
        }
        if (this.wmove) {
            wmove = false;
        } else {
            wmove = true;
        }
        invalidate();
    }


    public void move(String origin, String dest, ChessBoard b) {
        int[] o = b.getCoord(origin);
        int[] d = b.getCoord(dest);
        if (b.getchessboard()[o[0]][o[1]].getPiece().validmovement(origin, dest, b)) {
            Piece p = b.getchessboard()[o[0]][o[1]].getPiece();
            b.getchessboard()[d[0]][d[1]].setPiece(p);
            b.getchessboard()[d[0]][d[1]].getPiece().setRow(d[0]);
            b.getchessboard()[d[0]][d[1]].getPiece().setCol(d[1]);
            b.getchessboard()[o[0]][o[1]].rem();
        }
    }

    public boolean checkcheck(boolean wmove) {
        String dest = this.getKinglocation(wmove);
        if (dest == null) {
            return true;
        }
        // Toast.makeText(this.getContext(),dest,Toast.LENGTH_SHORT).show();

        if (wmove) {
            for (int i = 0; i < this.getchessboard().length; i++) {
                for (int j = 0; j < this.getchessboard().length; j++) {
                    if (!this.getchessboard()[i][j].empty) {
                        int[] a = {i, j};
                        String loc = this.getlocation(a);
                        if (getchessboard()[i][j].getPiece().getColor() == 'w' && getchessboard()[i][j].getPiece().validmovement(loc, dest, this)) {
                            //System.out.println(loc);
                            //System.out.println("Check!");

                            return true;
                        }

                        // Toast.makeText(this.getContext(),loc,Toast.LENGTH_SHORT).show();
                    }
                }
            }
        } else {
            for (int i = 0; i < getchessboard().length; i++) {
                for (int j = 0; j < getchessboard().length; j++) {
                    if (!getchessboard()[i][j].empty) {
                        int[] a = {i, j};
                        String loc = this.getlocation(a);


                        if (getchessboard()[i][j].getPiece().getColor() == 'b' && getchessboard()[i][j].getPiece().validmovement(loc, dest, this)) {
                            //System.out.println(loc);
                            //System.out.println("Check!");
                            return true;
                        }

                    }
                }
            }
        }
        return false;
        // return true;
    }


    public boolean checkmatecheck(boolean wmove) {
        //just safely check the the check status
        if (this.checkcheck(wmove)) {

            //System.out.println("Entering checkmate process");

            ChessBoard temp = new ChessBoard(this.getContext(), this);

            char color;
            if (wmove) {
                color = 'b';
            } else {
                color = 'w';
            }
            for (int i = 0; i < temp.chessboard.length; i++) {
                for (int j = 0; j < temp.chessboard.length; j++) {
                    if (!temp.getChessboard()[i][j].empty && temp.getChessboard()[i][j].getPiece().getColor() == color) {
                        int[] a = {i, j};
                        String pospieceloc = temp.getlocation(a);
                        Piece pospiece = temp.getchessboard()[i][j].getPiece();
                        for (int k = 0; k < temp.getchessboard().length; k++) {
                            for (int l = 0; l < temp.getchessboard().length; l++) {
                                int[] b = {k, l};
                                String pospiecemoveloc = temp.getlocation(b);
                                if (temp.getchessboard()[i][j].getPiece().validmovement(pospieceloc, pospiecemoveloc, temp)) {
                                    temp.move(pospieceloc, pospiecemoveloc, temp);
                                }
                                if (!temp.checkcheck(wmove)) {
                                    //c.printBoard();
                                    //System.out.println(pospieceloc+ " " + pospiecemoveloc);
                                    return false;
                                } else {
                                    temp = new ChessBoard(this.getContext(), this);

                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    public void endgame(final ChessBoard cv,String result) {
        //final String report=result;
        AlertDialog.Builder builder1 = new AlertDialog.Builder(cv.getContext());
        builder1.setMessage(result);
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                "Save",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        AlertDialog.Builder builder =new AlertDialog.Builder(cv.getContext());
                        final EditText fileName = new EditText(cv.getContext());
                        builder.setView(fileName);
                        builder.setNegativeButton(
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                    }

                                 });
                        builder.setPositiveButton(
                                "Save",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //Toast.makeText(cv.getContext(), fileName.getText().toString(), Toast.LENGTH_SHORT).show();
                                        GameData games = new GameData();
                                        try {
                                            games = games.read(getContext());
                                        } catch (ClassNotFoundException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        } catch (IOException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }

                                        String name = fileName.getText().toString();
                                        Game g = new Game(name);


                                        g.setMovelist(cv.moves);
                                        g.setEnd(true);
                                        //Game g = new Game("bvc",null);
                                        //String a = g.getMovelist().get(1);
                                        //Toast.makeText(getContext(), a, Toast.LENGTH_SHORT).show();
                                        games.AddGame(g);

                                        try {
                                            GameData.write(games,getContext());
                                        } catch (IOException ee) {
                                            ee.printStackTrace();
                                        }
                                        Intent intent = new Intent(getRootView().getContext(),list_games.class);
                                        getRootView().getContext().startActivity(intent);
                                        Play_chess.pc.finish();
                                    }
                                });
                        AlertDialog saveDialog = builder.create();
                        saveDialog.show();
                    }
                });
        builder1.setNegativeButton(
                "Quit",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(getRootView().getContext(),MainActivity.class);
                        getRootView().getContext().startActivity(intent);
                        Play_chess.pc.finish();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();


    }

}




