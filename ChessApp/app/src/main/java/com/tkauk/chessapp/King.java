package com.tkauk.chessapp;

/**
 * Created by tkauk on 4/20/2017.
 */

public class King extends Piece{
    boolean hasmoved=false;

    public King(String coord, char type, char color) {
        super(coord, type, color);
        // TODO Auto-generated constructor stub
    }

    public King(int row, int col, char type, char color){
        super(row,col,type,color);
    }

    public King(int row, int col, char type, char color, boolean hasmoved){
        super(row, col, type, color);
        this.hasmoved=hasmoved;
    }

    @Override
    public String toString(){
        if(color == 'w'){
            return "wK";
        }
        return "bK";

    }

    @Override
    public boolean validmovement(String origin, String dest, ChessBoard b) {

/*
        int[] o = new int[2];
        int[] d = new int[2];

        o[0] = b.origin.getRow();
        o[1] = b.origin.getCol();

        d[0] = b.dest.getRow();
        d[1] = b.dest.getCol();
        */
        int[] o = b.getCoord(origin);
        int[] d = b.getCoord(dest);
        if(b.getSquare(d[0],d[1]).getPiece() != null){
            if(b.getSquare(o[0],o[1]).getPiece().getColor()==b.getSquare(d[0],d[1]).getPiece().getColor()){
                return false;
            }
        }
        if(o[0]==d[0]&&Math.abs(o[1]-d[1])==1){
            hasmoved=true;
            return true;
        }else if(o[1]==d[1]&&Math.abs(o[0]-d[0])==1){
            hasmoved=true;
            return true;
        }else if(Math.abs(o[0]-d[0])==1 && Math.abs(o[1]-d[1])==1){
            hasmoved=true;
            return true;
        }else if(Math.abs(o[1]-d[1])==2&&o[0]==d[0]){
            char colorr =b.getSquare(o[0],o[1]).getPiece().getColor();
            boolean wwmove=false;
            if(colorr=='w'){
                wwmove=true;
            }
            if(!b.checkcheck(!wwmove))
                if(o[1]-d[1]==2&&!b.getSquare(o[0],o[1]-4).empty&&b.getchessboard()[o[0]][o[1]-4].getPiece().getType()=='R'&&b.getchessboard()[o[0]][o[1]-1].empty&&b.getchessboard()[o[0]][o[1]-2].empty&&b.getchessboard()[o[0]][o[1]-3].empty){
                    Rook r = (Rook)(b.getchessboard()[o[0]][o[1]-4].getPiece());
                    King k = (King)(b.getchessboard()[o[0]][o[1]].getPiece());
                    if(!r.hasmoved&&!k.hasmoved){
                        boolean kinggocheck=false;
                        ChessBoard c=new ChessBoard(b.getContext(),b);
                        for(int i=1;i<=2;i++){
                            int[] e=o;
                            e[1]-=1;
                            String fakeloc = c.getlocation(e);
                            c.straightmove(origin,fakeloc);
                            if(c.checkcheck(!wwmove)){
                                kinggocheck=true;
                                break;
                            }else{
                                c=new ChessBoard(b.getContext(),b);
                            }

                        }
                        if(!kinggocheck){
                            int[] e=d;
                            e[1]+=1;
                            int[] f=o;
                            f[1]-=2;
                            String rookorigin=b.getlocation(f);
                            String rookdest=b.getlocation(e);
                            b.straightmove(rookorigin,rookdest);
                            this.hasmoved=true;
                            return true;
                        }

                    }
                }else if(o[1]-d[1]==-2&&!b.getSquare(o[0],o[1]+3).empty&&b.getchessboard()[o[0]][o[1]+3].getPiece().getType()=='R'&&b.getchessboard()[o[0]][o[1]+1].empty&&b.getchessboard()[o[0]][o[1]+2].empty){
                    Rook r = (Rook)(b.getchessboard()[o[0]][o[1]+3].getPiece());
                    King k = (King)(b.getchessboard()[o[0]][o[1]].getPiece());
                    if(!r.hasmoved&&!k.hasmoved){
                        boolean kinggocheck=false;
                       ChessBoard c=new ChessBoard(b.getContext(),b);
                       for(int i=1;i<=2;i++){
                            int[] e=o;
                            e[1]=e[1]+1;
                            String fakeloc = c.getlocation(e);
                            //System.out.println(origin);
                            //System.out.println(fakeloc);
                            c.straightmove(origin,fakeloc);
                            //c.printBoard();
                            if(c.checkcheck(!wwmove)){
                                kinggocheck=true;
                                break;
                            }else{
                                c=new ChessBoard(b.getContext(),b);
                            }
                        }
                        if(!kinggocheck){
                            int[] x=d;
                            x[1]-=1;
                            int[] f=o;
                            f[1]=o[1]+1;
                            String rookorigin=b.getlocation(f);
                            String rookdest=b.getlocation(x);
                            //System.out.println(rookorigin);
                            //System.out.println(rookdest);
                            b.straightmove(rookorigin,rookdest);
                            //b.printBoard();
                            this.hasmoved=true;
                            return true;
                        }
                    }

                }

        }
        return false;
    }


}

