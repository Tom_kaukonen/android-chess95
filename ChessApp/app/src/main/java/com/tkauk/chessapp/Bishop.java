package com.tkauk.chessapp;

/**
 * Created by tkauk on 4/20/2017.
 */

public class Bishop extends Piece{

    public Bishop(String coord, char type, char color) {
        super(coord, type, color);
        // TODO Auto-generated constructor stub
    }


    public Bishop(int row, int col, char type, char color){
        super(row,col,type,color);
    }


    @Override
    public String toString(){
        if(color == 'w'){
            return "wB";
        }
        return "bB";

    }

    @Override
    public boolean validmovement(String origin, String dest, ChessBoard b) {
        // TODO Auto-generated method stub
        int[] o = b.getCoord(origin);
        int[] d = b.getCoord(dest);
        /*
        int[] o = new int[2];
        int[] d = new int[2];

        o[0] = b.origin.getRow();
        o[1] = b.origin.getCol();

        d[0] = b.dest.getRow();
        d[1] = b.dest.getCol();
    */
        if(b.getSquare(d[0],d[1]).getPiece()!=null){
            if(b.getSquare(o[0],o[1]).getPiece().getColor()==b.getSquare(d[0],d[1]).getPiece().getColor()){
                return false;
            }
        }

        if(Math.abs(o[0]-d[0])==Math.abs(o[1]-d[1])){

            if(o[0]<d[0] && o[1]<d[1]){
                for(int i=1;i<Math.abs(o[0]-d[0]);i++){
                    if(b.getSquare(o[0]+i, o[1]+i).getPiece() != null){
                        return false;
                    }
                }
            }else if(o[0]<d[0] && o[1]>d[1]){
                for(int i=1;i<Math.abs(o[0]-d[0]);i++){
                    if(b.getSquare(o[0]+i, o[1]-i).getPiece() != null){
                        return false;
                    }
                }
            }else if(o[0]>d[0] && o[1]<d[1]){
                for(int i=1;i<Math.abs(o[0]-d[0]);i++){
                    if(b.getSquare(o[0]-i, o[1]+i).getPiece() != null){
                        return false;
                    }
                }
            }else if(o[0]>d[0] && o[1]>d[1]){
                for(int i=1;i<Math.abs(o[0]-d[0]);i++){
                    if(b.getSquare(o[0]-i, o[1]-i).getPiece() != null){
                        return false;
                    }
                }
            }
            //System.out.println("Congrats you moved the bishop");
            return true;
        }
        return false;
    }
}
