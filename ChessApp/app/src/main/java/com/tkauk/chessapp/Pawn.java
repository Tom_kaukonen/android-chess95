package com.tkauk.chessapp;

import android.widget.Toast;

/**
 * Created by tkauk on 4/20/2017.
 */

public class Pawn extends Piece{

    public boolean hasMoved = false;
    public boolean movedtwoSquares =false;
    //keep track of when piece was last moved, for enpassant purposes
    public int move = 0;

    public Pawn(String coord, char type, char color) {
        super(coord, type, color);
        // TODO Auto-generated constructor stub
    }


    public Pawn(int row, int col, char type, char color){
        super(row,col,type,color);
    }

    public Pawn(int row, int col, char type, char color, boolean hasMoved, boolean movedtwoSquares){
        super(row,col,type,color);
        this.hasMoved=hasMoved;
        this.movedtwoSquares=movedtwoSquares;
    }

    public Pawn(int row, int col, char type, char color, boolean hasMoved, boolean movedtwoSquares, int move){
        super(row,col,type,color);
        this.hasMoved=hasMoved;
        this.movedtwoSquares=movedtwoSquares;
        this.move = move;
    }
    @Override
    public boolean validmovement(String origin, String dest, ChessBoard b){

      /*  int[] o = new int[2];
        int[] d = new int[2];

        o[0] = b.origin.getRow();
        o[1] = b.origin.getCol();

        d[0] = b.dest.getRow();
        d[1] = b.dest.getCol();
    */
        int[] o = b.getCoord(origin);
        int[] d = b.getCoord(dest);
        if(b.getSquare(d[0],d[1]).getPiece()!=null){
            if(b.getSquare(o[0],o[1]).getPiece().getColor()==b.getSquare(d[0],d[1]).getPiece().getColor()){
                return false;
            }
        }
        // allows it to move twice
        //basic pawn capture

        //enpassant
        if(b.getchessboard()[d[0]][d[1]].empty&&Math.abs(o[1]-d[1])==1&& Math.abs(d[0]-o[0])==1&&!b.getSquare(o[0],d[1]).empty){
           // Toast.makeText(b.getContext(), "oh no", Toast.LENGTH_SHORT).show();

            //System.out.println("entering enpassant process");

            if(b.getchessboard()[o[0]][d[1]].getPiece().type=='P' && (b.getchessboard()[o[0]][o[1]].getPiece().getColor() != b.getchessboard()[o[0]][d[1]].getPiece().getColor())){
                Pawn p = (Pawn) b.getchessboard()[o[0]][d[1]].getPiece();
                //System.out.println("This is the pawn's move: " + p.move);

                if(p.movedtwoSquares && p.move == ChessBoard.movecount -1){
                    //System.out.println("This is the pawn's move: " + p.move);
                    //System.out.println("congrats you performed enpassant!");
                    b.getchessboard()[o[0]][d[1]].rem();

                    return true;

                }
                //System.out.println("This is the pawn's move: " + p.move);
                return false;

            }
        }



        //cant move 2 or more spaces
        if(this.hasMoved && Math.abs(o[0] - d[0]) > 1){
            return false;
        }

        if(!this.hasMoved && Math.abs(o[0]-d[0])>2){
            return false;
        }
        //CANT MOVE 3 OR MORE SPACES
        if(!this.hasMoved && Math.abs(o[1] - d[1]) > 2){
            return false;
        }




        if(!b.getchessboard()[d[0]][d[1]].empty&&Math.abs(d[1]-o[1])==1){
            //System.out.println("Congrates your pawn took a piece");
            //Toast.makeText(b.getContext(), "error", Toast.LENGTH_SHORT).show();
            if(o[0] == d[0]){
                return false;
            }

            if(Math.abs(o[0] - d[0]) == 2){
                return false;
            }

            if(this.getColor() == 'w' && d[0] > o[0]){
                return false;
            }

            if(this.getColor() == 'b' && d[0] < o[0]){
                return false;
            }

            return true;
        }
        //pawn wants to move up and theres a piece in the way


        if(!b.getchessboard()[d[0]][d[1]].empty){
            return false;
        }
        //black pawn cant move backwards or sideways
        if(this.color == 'b' && d[0] <= o[0]){
            return false;
        }

        //white cant move backwards or sideways
        if(this.color == 'w' && d[0] >= o[0]){
            return false;
        }
        //prevents white from moving pawns multiple spots in diagnols
        if(Math.abs(d[1] - o[1]) > 0 && b.getchessboard()[d[0]][d[1]].empty){
            //System.out.println("ERROR: pawn cant move diagonally in noncaptures");
            return false;
        }



        if(Math.abs(o[0] - d[0]) <= 2  && !this.hasMoved){
            //black pawn wants to move 2 spaces checks if piece in front



            if(Math.abs(o[0] - d[0]) == 2){
                //Toast.makeText(b.getContext(), "moving pawn", Toast.LENGTH_SHORT).show();

                if(this.color == 'b' &&  !b.getchessboard()[d[0] - 1][d[1]].empty){
                    //System.out.println("theres a piece in the way");
                   // Toast.makeText(b.getContext(), "oyyyyy vay", Toast.LENGTH_SHORT).show();
                    return false;
                }
                if(this.color == 'w' &&  !b.getchessboard()[d[0] + 1][d[1]].empty){
                    //System.out.println("theres a piece in the way");
                    //Toast.makeText(b.getContext(), "oyyyyy nay", Toast.LENGTH_SHORT).show();
                    return false;
                }
                this.movedtwoSquares = true;
                this.move = ChessBoard.movecount;

                //System.out.println("This pawn moved two squares: " + move);

            }
           //Toast.makeText(b.getContext(), "pawn should move", Toast.LENGTH_SHORT).show();

            this.hasMoved = true;
            //	System.out.println("The pawn has moved!!!!");
            return true;

        }



        return true;


    }


    @Override
    public String toString(){
        if(color == 'w'){
            return "wP";
        }
        return "bP";

    }


}
