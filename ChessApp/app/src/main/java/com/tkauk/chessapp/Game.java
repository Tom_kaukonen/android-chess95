package com.tkauk.chessapp;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by zeyuh on 2017/4/21.
 */

public class Game implements Serializable {
    private String name;
    private boolean end;
    public ArrayList<Move> movelist;
    //private int count;
    //private ArrayList<String> move;
    //private boolean wmove;
    private Calendar date;

    public Game() {
        this.movelist = null;
        this.name = null;
        //this.wmove=true;
        //this.count=0;
        //this.move=new ArrayList<String>();
        this.end = false;
    }

    public Game(String name) {
        this.name = name;
        // this.count=count;
        end = false;
        this.movelist = new ArrayList<Move>();
        // this.move=move;
        this.date = Calendar.getInstance();
        this.date.set(Calendar.MILLISECOND, 0);
    }

    public Game(String name, ArrayList<Move> movesString) {
        this.name = name;
        //save = true;
        this.date = Calendar.getInstance();
        this.date.set(Calendar.MILLISECOND, 0);
        this.movelist = movesString;
    }

    public ArrayList<Move> getMovelist() {
        return movelist;
    }


    public void setMovelist(ArrayList<Move> movelist) {

       this.movelist = movelist;
        }


    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        String dt = sdf.format(this.getdate().getTime());
        return this.getName() + "(" + dt + ")";
    }

    public String getName() {
        return name;
    }

    public Calendar getdate() {
        return date;
    }

    public void setEnd(boolean end){
        this.end=end;
    }
    public boolean getEnd(){
        return end;
    }
    public static Comparator<Game> GameNameComparator = new Comparator<Game>(){
        public int compare(Game g1, Game g2){
            String Name1 = g1.getName().toLowerCase();
            String Name2 = g2.getName().toLowerCase();

            return Name1.compareTo(Name2);
        }
    };

    public static Comparator<Game> GameDateComparator = new Comparator<Game>(){
        public int compare(Game g1, Game g2){
            Calendar date1 = g1.getdate();
            Calendar date2 = g2.getdate();
            if(date1.before(date2)){
                return -1;
            }else{
                return 1;
            }
        }
    };
}


