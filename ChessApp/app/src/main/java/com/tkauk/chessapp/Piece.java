package com.tkauk.chessapp;

import java.io.Serializable;

/**
 * Created by tkauk on 4/20/2017.
 */

public abstract class Piece implements Serializable{

    //fields shared by all pieces go here
    //algebraic notation of piece coordinate
    public String coord;
    //indicates the piece type
    char type;
    //indicates piece color
    char color;

    int row;

    int col;

    /**
     * Piece constructor with coord type and color
     * @param coord
     * @param type
     * @param color
     */
    public Piece(String coord, char type,  char color){
        this.coord = coord;
        this.type = type;
        this.color = color;

        this.row = Piece.getRow(coord);
        this.col = Piece.getColumn(coord);
    }


    /**
     * Piece constructor with row col type and color
     * @param row
     * @param col
     * @param type
     * @param color
     */
    public Piece(int row, int col, char type, char color){
        this.row = row;
        this.col = col;
        this.type = type;
        this.color = color;
        this.coord = Piece.genCoord(row, col);



    }



    /**
     * Gets the row number
     * @return int representing the row
     */
    public int getRow() {
        return row;
    }

    /**
     *
     * @param row sets the row index
     */

    public void setRow(int row) {
        this.row = row;
    }

    /**
     *
     * @return col
     */

    public int getCol() {
        return col;
    }

    /**
     *
     * @param col ddets the col index
     */

    public void setCol(int col) {
        this.col = col;
    }

    /**
     *
     * @param row
     * @param column
     * @return String in algebraic chess notation
     */
    public static String genCoord(int row, int column){

        char let = (char) (row + 97);
        int col = 8 - column;
        //int col = column;

        String result=let+""+col;

        return result;
    }

    /**
     *
     * @return String of coordinate
     */
    public String getCoord() {
        return coord;
    }

    /**
     *
     * @param coord
     * @return int representing the row
     */
    private static int getRow(String coord){
        char letter = coord.charAt(0);

        int row = letter - 97;

        return row;
    }

    /**
     *
     * @param coord
     * @return int representing column
     */
    private static int getColumn(String coord){
        char num = coord.charAt(1);

        int col  = num - 49;

        return col;
    }


    /**
     *
     * @param coord setting string coordinate
     */
    public void setCoord(String coord) {
        this.coord = coord;
    }


    /**
     *
     * @return char of piece type
     */
    public char getType() {
        return type;
    }

    /**
     *
     * @param type sets the piece type
     */
    public void setType(char type) {
        this.type = type;
    }

    /**
     *
     * @return char of piece color
     */
    public char getColor() {
        return color;
    }

    /**
     *
     * @param c sets piece color
     */
    public void setColor(char c) {
        this.color = c;
    }

    /**
     * @return String representation of given piece
     */
    public abstract String toString();
    /**
     * The check to see a valid piece movement
     * @param origin
     * @param dest
     * @param b
     * @return boolean indicating if a given piece can move to a given square
     */
    public abstract boolean validmovement(String origin, String dest,ChessBoard b) ;

}