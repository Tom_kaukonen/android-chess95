package com.tkauk.chessapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.constraint.solver.widgets.Rectangle;

import java.io.Serializable;

/**
 * Created by tkauk on 4/20/2017.
 */

public class Square implements Serializable{
    //used for drawing individual squares and pieces on the squares
    private int col;
    private int row;

    private boolean isDark;

    private Paint Scolor;
    private Rect r;
    public boolean empty = true;
    public Piece p;
    public boolean Selected1 = true;
    public boolean isSelected2 = false;

    public boolean isSelected() {
        return isSelected;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {

        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    private boolean isSelected = false;


    public Square(final int row, final int col) {
        this.col = col;
        this.row = row;
        this.p = null;
        this.empty = true;

        this.Scolor = new Paint();

        Scolor.setColor(isDark() ? Color.parseColor("#724d45") : Color.parseColor("#ffdaac"));
    }

    public Paint getScolor() {
        return Scolor;
    }

    public void setColor(Paint scolor) {
        Scolor = scolor;
    }

    public Square(Piece p, final int col, final int row) {
        this.col = col;
        this.row = row;
        this.p = p;
        this.empty = false;
        this.Scolor = new Paint();
        Scolor.setColor(isDark() ? Color.BLACK : Color.WHITE);
    }

    public Piece getPiece() {
        return p;
    }

    public void setPiece(Piece p) {
        this.p = p;
        this.empty = false;
    }

    public void draw(Canvas canvas, String piece, Context c) {

        if(Selected1){
        canvas.drawRect(r,Scolor);}
        else{
            canvas.drawRect(r,new Paint(Color.GREEN));
        }


        if(!piece.equals("  ") && !piece.equals("##")){
            Bitmap pieceBM = null;
            switch(piece) {
                case "wP": {
                    pieceBM = BitmapFactory.decodeResource(c.getResources(), R.drawable.white_pawn);
                    break;
                }
                case "wK": {
                    pieceBM = BitmapFactory.decodeResource(c.getResources(), R.drawable.white_king);
                    break;
                }
                case "wB": {
                    pieceBM = BitmapFactory.decodeResource(c.getResources(), R.drawable.white_bishop);
                    break;
                }
                case "wN": {
                    pieceBM = BitmapFactory.decodeResource(c.getResources(), R.drawable.white_knight);
                    break;
                }
                case "wR": {
                    pieceBM = BitmapFactory.decodeResource(c.getResources(), R.drawable.white_rook);
                    break;
                }
                case "wQ": {
                    pieceBM = BitmapFactory.decodeResource(c.getResources(), R.drawable.white_queen);
                    break;
                }
                case "bP": {
                    pieceBM = BitmapFactory.decodeResource(c.getResources(), R.drawable.black_pawn);
                    break;
                }
                case "bK": {
                    pieceBM = BitmapFactory.decodeResource(c.getResources(), R.drawable.black_king);
                    break;
                }
                case "bB": {
                    pieceBM = BitmapFactory.decodeResource(c.getResources(), R.drawable.black_bishop);
                    break;
                }
                case "bN": {
                    pieceBM = BitmapFactory.decodeResource(c.getResources(), R.drawable.black_knight);
                    break;
                }
                case "bR": {
                    pieceBM = BitmapFactory.decodeResource(c.getResources(), R.drawable.black_rook);
                    break;
                }
                case "bQ": {
                    pieceBM = BitmapFactory.decodeResource(c.getResources(), R.drawable.black_queen);
                    break;
                }
            }
            canvas.drawBitmap(pieceBM,null,r,null);
        }

    }


    public boolean isDark() {
        return (col + row) % 2 == 1;
    }

    public void setTileRect(Rect tileRect) {
        this.r = tileRect;
    }


    public boolean isTouched(final int x, final int y) {
        return r.contains(x, y);
    }

    public void handleTouch(){
        this.Scolor.setColor(Color.parseColor("#00aa00"));



    }

    @Override
    public String toString(){
        if(empty){
            if(!isDark()){
                return "  ";
            }
            else{
                return "##";
            }
        }
        //System.out.println("calling piece");
        return p.toString();

    }

    public void rem(){
        if(this.p !=null){
            this.p  =null;
            this.empty=true;
        }else{
            return;
        }

    }

    public boolean rightmove(ChessBoard board){
        if(board.movecount%2==0&&board.wmove&&this.getPiece().getColor()=='w'){
            return true;
        }
        if(board.movecount%2==1&&!board.wmove&&this.getPiece().getColor()=='b'){
            return true;
        }
        return false;
    }

}
