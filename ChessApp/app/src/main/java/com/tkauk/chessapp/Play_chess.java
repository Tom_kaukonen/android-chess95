package com.tkauk.chessapp;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.NoSuchElementException;

public class Play_chess extends AppCompatActivity {

    RelativeLayout rel;
    public static Activity pc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.chessboard_layout);
        pc=this;
        rel = new RelativeLayout(this);
       // Game a = new Game();
        final ChessBoard cv = new ChessBoard(this);
        //a.setChessBoard(cv);
        final String g_name = getIntent().getStringExtra("GAME");
        if(g_name!=null){
            int currindex = 0;
            GameData glist = new GameData();
            try {
                glist = GameData.read(rel.getContext());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            for(int i =0; i <glist.getGames().size(); i++){
                if(glist.getGames().get(i).getName().equals(g_name)){
                    currindex = i;
                }


            }

            final Game rep_game = glist.getGames().get(currindex);
            ArrayList<Move> mov=rep_game.getMovelist();
            for(int i=0;i<mov.size();i++){
                Move m = mov.get(i);
                cv.straightmove(m.getOrigin(),m.getDest());
                cv.moves.add(new Move(m.getOrigin(), m.getDest()));
                cv.movecount++;
                cv.wmove=!cv.wmove;

            }
            cv.prev=null;
            cv.origin=null;
        }
        int v = View.generateViewId();
        cv.setId(v);
        //a.getBoard().setId(1);
        rel.addView(cv);

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) cv.getLayoutParams();
        params.setMargins(10, 10, 10, 10); //substitute parameters for left, top, right, bottom

        params.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        cv.setLayoutParams(params);


        RelativeLayout.LayoutParams lprams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

        TableLayout t = new TableLayout(this);


        TableRow tr1 = new TableRow(this);
        TableRow tr2 = new TableRow(this);

        lprams.addRule(RelativeLayout.ALIGN_BOTTOM, v);
        //lprams.addRule(RelativeLayout.ALIGN_LEFT);
        t.setLayoutParams(lprams);
        //tr1.setLayoutParams(lprams);
        //tr1.setLayoutParams(lprams);
        Button undo = new Button(this);
        undo.setText("Undo");
        // undo.setLayoutParams(lprams);
        undo.setWidth(30);
        undo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ChessBoard prevstate = cv.prev;
                if (prevstate == null) {
                    return;
                }
                cv.renderPrev(prevstate);

                //<-- See This!
            }
            // undo();
            //code for ai button action goes here

        });
        tr1.addView(undo);

        Button Ai = new Button(this);
        Ai.setText("Ai");
        //btn2.setLayoutParams(lprams);
        Ai.setWidth(30);
        Ai.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (cv.origin != null) {
                    cv.origin.Selected1 = true;
                    cv.origin = null;
                }

                try {
                    cv.AutomaticMove();

                } catch (NoSuchElementException e) {
                    Toast.makeText(rel.getContext(), "The computer has no moves for you.",
                            Toast.LENGTH_SHORT).show();
                }
                // ai();
                //code for ai button action goes here
            }
        });


        tr1.addView(Ai);


        Button resign = new Button(this);
        resign.setText("resign");
        //btn2.setLayoutParams(lprams);
        resign.setWidth(30);
        resign.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String resign=null;
                if(cv.wmove){
                    resign="White Resigns, Black wins";
                }
                else{
                    resign="Black Resigns,White wins";
                }
                cv.endgame(cv,resign);// resign();
                //code for ai button action goes here
            }
        });
        tr2.addView(resign);

        Button draw = new Button(this);
        draw.setText("Draw");
        draw.setWidth(30);
        draw.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(v.getContext());
                dialog.setContentView(R.layout.draw);
                dialog.show();
                Button accept = (Button) dialog.findViewById(R.id.Accept);
                Button decline = (Button) dialog.findViewById(R.id.Decline);
                accept.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        cv.endgame(cv,"Tie");
                    }
                });
                decline.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
                // draw();
                //code for ai button action goes here
            }
        });
        tr2.addView(draw);

        Button save = new Button(this);
        save.setText("Save");
        save.setWidth(30);
        save.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // save();
                //code for ai button action goes here
                android.app.AlertDialog.Builder builder =new android.app.AlertDialog.Builder(cv.getContext());
                final EditText fileName = new EditText(cv.getContext());
                builder.setView(fileName);
                builder.setNegativeButton(
                        "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }

                        });
                builder.setPositiveButton(
                        "Save",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //Toast.makeText(cv.getContext(), fileName.getText().toString(), Toast.LENGTH_SHORT).show();
                                GameData games=new GameData();
                                try {
                                    games = games.read(cv.getContext());
                                } catch (ClassNotFoundException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }

                                String name = fileName.getText().toString();
                                Game g = new Game(name);


                                g.setMovelist(cv.moves);
                                //Game g = new Game("bvc",null);
                                //String a = g.getMovelist().get(1);
                                //Toast.makeText(getContext(), a, Toast.LENGTH_SHORT).show();
                                games.AddGame(g);

                                try {
                                    GameData.write(games,cv.getContext());
                                } catch (IOException ee) {
                                    ee.printStackTrace();
                                }
                                Intent intent = new Intent(cv.getContext(),list_games.class);
                                cv.getContext().startActivity(intent);
                                Play_chess.pc.finish();
                            }
                        });
                android.app.AlertDialog saveDialog = builder.create();
                saveDialog.show();

            }
        });
        tr2.addView(save);

        t.addView(tr1);
        t.addView(tr2);
        rel.addView(t);
        // mLinearLayout.setDividerPadding(10);
        // mLinearLayout.

        //mLinearLayout.addView(row);
        setContentView(rel);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
