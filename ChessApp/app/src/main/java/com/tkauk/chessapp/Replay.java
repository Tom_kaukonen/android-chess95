package com.tkauk.chessapp;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

import java.io.IOException;
import java.util.NoSuchElementException;

/**
 * Created by zeyuh on 2017/4/30.
 */

public class Replay extends AppCompatActivity {
    RelativeLayout rel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        rel = new RelativeLayout(this);

        final String g_name = getIntent().getStringExtra("GAME");

        int currindex = 0;
        GameData glist = new GameData();
        try {
             glist = GameData.read(rel.getContext());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        for(int i =0; i <glist.getGames().size(); i++){
            if(glist.getGames().get(i).getName().equals(g_name)){
                currindex = i;
            }


        }

        final Game rep_game = glist.getGames().get(currindex);

        final Chessboard_replay cv = new Chessboard_replay(this, rep_game.getMovelist());
        int v = View.generateViewId();
        cv.setId(v);
        rel.addView(cv);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) cv.getLayoutParams();
        params.setMargins(10, 10, 10, 10); //substitute parameters for left, top, right, bottom

        params.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        cv.setLayoutParams(params);


        RelativeLayout.LayoutParams lprams = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        final TableLayout t = new TableLayout(this);


        TableRow tr1 = new TableRow(this);
        Button back = new Button(this);
        back.setText("Back");
        // undo.setLayoutParams(lprams);
        back.setWidth(120);
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Chessboard_replay prevstate = cv.prev;
                if (prevstate == null) {
                    return;
                }

                    cv.renderPrev(prevstate);



                //<-- See This!
            }
            // undo();
            //code for ai button action goes here

        });
        tr1.addView(back);

        Button forward = new Button(this);
        forward.setText("Next");
        //btn2.setLayoutParams(lprams);
        forward.setWidth(120);
        forward.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(cv.movecount == cv.moves.size()){
                    return;
                }
                cv.renderNext();
                // ai();
                //code for ai button action goes here
            }
        });


        tr1.addView(forward);
        Button playback = new Button(this);
        playback.setText("Continue");
        playback.setWidth(80);
        playback.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                if(rep_game.getEnd()){
                    Toast.makeText(getApplicationContext(),"This game has been ended",Toast.LENGTH_SHORT).show();
                    return;
                }
                else {
                    Intent intent = new Intent(getApplicationContext(),Play_chess.class);
                    intent.putExtra("GAME", g_name);
                    startActivity(intent);
                    finish();
                }

            }
        });
        tr1.addView(playback);
        lprams.addRule(RelativeLayout.ALIGN_BOTTOM, v);
        //lprams.addRule(RelativeLayout.ALIGN_LEFT);
        t.setLayoutParams(lprams);
        t.addView(tr1);
        rel.addView(t);

        setContentView(rel);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
