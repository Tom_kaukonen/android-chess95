package com.tkauk.chessapp;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.View;

import java.util.ArrayList;

/**
 * Created by tkauk on 4/30/2017.
 */

public class Chessboard_replay extends View{
    public Square[][] chessboard;
    //must be defined here
    //overwrite the onDraw method
    public int num_selected = 0;
    private int x0 = 0;
    private int y0 = 0;
    private int squareSize = 0;
    Square origin = null;
    Square dest = null;
    public static int movecount = 0;
    boolean wmove = true;
    public Chessboard_replay prev;
    public char promote = 'X';
    public boolean promoting;
    public ArrayList<Move> moves;


    public Chessboard_replay(Context context, ArrayList<Move> m) {
        super(context);
        this.chessboard = new Square[8][8];
        this.moves = m;
        setSquares();
        setBlack();
        setWhite();
        this.prev = null;
        this.promoting = false;
        this.movecount = 0;
        // this.origin.Selected1 = true;
        this.origin = null;
        this.dest=null;


    }

    public Chessboard_replay(Context context, Chessboard_replay another) {
        super(another.getContext());


        chessboard = new Square[8][8];
        setSquares();
        for (int i = 0; i < another.chessboard.length; i++) {
            for (int j = 0; j < another.chessboard.length; j++) {
                if (!another.getSquare(i, j).empty) {
                    char color = another.getSquare(i, j).getPiece().getColor();
                    char type = another.getSquare(i, j).getPiece().getType();
                    if (type == 'P') {
                        Pawn r = (Pawn) another.getSquare(i, j).getPiece();
                        boolean hasMoved = r.hasMoved;
                        boolean movedtwoSquares = r.movedtwoSquares;
                        int move = r.move;
                        // Toast.makeText(this.getContext(), "movecount: " + r.move, Toast.LENGTH_SHORT).show();
                        chessboard[i][j].setPiece(new Pawn(i, j, 'P', color, hasMoved, movedtwoSquares, move));
                    } else if (type == 'K') {
                        King r = (King) another.getSquare(i, j).getPiece();
                        boolean hasMoved = r.hasmoved;
                        chessboard[i][j].setPiece(new King(i, j, 'K', color, hasMoved));
                    } else if (type == 'N') {
                        chessboard[i][j].setPiece(new Knight(i, j, 'N', color));
                    } else if (type == 'Q') {
                        chessboard[i][j].setPiece(new Queen(i, j, 'Q', color));
                    } else if (type == 'R') {
                        Rook r = (Rook) another.getSquare(i, j).getPiece();
                        boolean hasmoved = r.hasmoved;
                        chessboard[i][j].setPiece(new Rook(i, j, 'R', color, hasmoved));
                    } else if (type == 'B') {
                        chessboard[i][j].setPiece(new Bishop(i, j, 'B', color));
                    }
                }
            }
        }
    }




    private void setSquares() {
        for (int r = 0; r < 8; r++) {
            for (int c = 0; c < 8; c++) {
                chessboard[r][c] = new Square(r, c);
            }
        }
    }

    private void setWhite() {
        //chessboard[1][2].setPiece(new Pawn(1,2,'P', 'w'));
        //chessboard[1][4].setPiece(new King(1,5,'K','w'));
        //chessboard[7][5].setPiece(new Queen(6,6,'Q','w'));
        //chessboard[7][0].setPiece(new King(7,0,'K','w'));
        for (int i = 0; i < chessboard.length; i++) {
            for (int j = 0; j < chessboard.length; j++) {
                //its a pawn
                if (i == 6) {

                    //	System.out.println("creating pawns");
                    chessboard[i][j].setPiece(new Pawn(i, j, 'P', 'w'));
                }

                if ((i == 7 && j == 0) || (i == 7 && j == 7)) {

                    chessboard[i][j].setPiece(new Rook(i, j, 'R', 'w'));
                }

                if ((i == 7 && j == 1) || (i == 7 && j == 6)) {

                    chessboard[i][j].setPiece(new Knight(i, j, 'N', 'w'));
                }

                if ((i == 7 && j == 2) || (i == 7 && j == 5)) {

                    chessboard[i][j].setPiece(new Bishop(i, j, 'B', 'w'));
                }


                if ((i == 7 && j == 3)) {

                    chessboard[i][j].setPiece(new Queen(i, j, 'Q', 'w'));
                }

                if ((i == 7 && j == 4)) {

                    chessboard[i][j].setPiece(new King(i, j, 'K', 'w'));
                }


            }
        }

    }


    private void setBlack() {
        //chessboard[6][3].setPiece(new Queen(6,3,'Q','b'));
        //chessboard[0][0].setPiece(new King(0,0,'K','b'));
        for (int i = 0; i < chessboard.length; i++) {
            for (int j = 0; j < chessboard.length; j++) {
                //its a pawn

                if (i == 1) {
                    chessboard[i][j].setPiece(new Pawn(i, j, 'P', 'b'));
                }

                if ((i == 0 && j == 0) || (i == 0 && j == 7)) {

                    chessboard[i][j].setPiece(new Rook(i, j, 'R', 'b'));
                }

                if ((i == 0 && j == 1) || (i == 0 && j == 6)) {

                    chessboard[i][j].setPiece(new Knight(i, j, 'N', 'b'));
                }

                if ((i == 0 && j == 2) || (i == 0 && j == 5)) {

                    chessboard[i][j].setPiece(new Bishop(i, j, 'B', 'b'));
                }


                if ((i == 0 && j == 3)) {

                    chessboard[i][j].setPiece(new Queen(i, j, 'Q', 'b'));
                }

                if ((i == 0 && j == 4)) {

                    chessboard[i][j].setPiece(new King(i, j, 'K', 'b'));
                }


            }

        }


    }

    public Square[][] getChessboard() {
        return chessboard;
    }

    public void setChessboard(Square[][] chessboard) {
        this.chessboard = chessboard;
    }

    public Square getSquare(int row, int col) {
        return chessboard[row][col];
    }

    public Square[][] getchessboard() {
        return this.chessboard;
    }


    public int[] getCoord(String location) {
        int[] ret = new int[2];
        char letter = location.charAt(0);
        char row = location.charAt(1);

        int col = letter - 97;

        int r = Math.abs(row - 56);

        ret[0] = r;
        ret[1] = col;

        return ret;


    }


    public void setPrev(Chessboard_replay prev) {
        this.prev = prev;
    }

    public void renderPrev(Chessboard_replay another) {

        //ArrayList<Move> moveL = this.moves;
        //this. = new ChessBoard(this.getContext());
        //TO DO:  allow pawns to capture enpassant;


        chessboard = new Square[8][8];
        setSquares();
        for (int i = 0; i < another.chessboard.length; i++) {
            for (int j = 0; j < another.chessboard.length; j++) {
                if (!another.getSquare(i, j).empty) {
                    char color = another.getSquare(i, j).getPiece().getColor();
                    char type = another.getSquare(i, j).getPiece().getType();
                    if (type == 'P') {
                        Pawn r = (Pawn) another.getSquare(i, j).getPiece();
                        boolean hasMoved = r.hasMoved;
                        boolean movedtwoSquares = r.movedtwoSquares;
                        int move = r.move;
                        chessboard[i][j].setPiece(new Pawn(i, j, 'P', color, hasMoved, movedtwoSquares, move));
                    } else if (type == 'K') {
                        King r = (King) another.getSquare(i, j).getPiece();
                        boolean hasMoved = r.hasmoved;
                        chessboard[i][j].setPiece(new King(i, j, 'K', color, hasMoved));
                    } else if (type == 'N') {
                        chessboard[i][j].setPiece(new Knight(i, j, 'N', color));
                    } else if (type == 'Q') {
                        chessboard[i][j].setPiece(new Queen(i, j, 'Q', color));
                    } else if (type == 'R') {
                        Rook r = (Rook) another.getSquare(i, j).getPiece();
                        boolean hasmoved = r.hasmoved;
                        chessboard[i][j].setPiece(new Rook(i, j, 'R', color, hasmoved));
                    } else if (type == 'B') {
                        chessboard[i][j].setPiece(new Bishop(i, j, 'B', color));
                    }
                }
            }
        }

        this.wmove = !wmove;
        this.movecount--;
        this.prev = another.prev;

        invalidate();


    }

    public void renderNext() {
        Chessboard_replay temp = new Chessboard_replay(this.getContext(),this);
        temp.setPrev(this.prev);
        this.prev = temp;

        Move m = this.moves.get(movecount);

        straightmove(m.getOrigin(), m.getDest());

        //ArrayList<Move> moveL = this.moves;
        //this. = new ChessBoard(this.getContext());
        //TO DO:  allow pawns to capture enpassant;





        this.wmove = !wmove;
        this.movecount++;

        invalidate();


    }





    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int width = getWidth();
        int height = getHeight();
        this.squareSize = Math.min(
                (width / 8),
                (height / 8)
        );


        for (int r = 0; r < 8; r++) {
            for (int c = 0; c < 8; c++) {
                int xCoord = getXCoord(c);
                int yCoord = getYCoord(r);

                Rect tileRect = new Rect(
                        xCoord,               // left
                        yCoord,               // top
                        xCoord + squareSize,  // right
                        yCoord + squareSize   // bottom
                );
                chessboard[r][c].setTileRect(tileRect);
                String type = chessboard[r][c].toString();
                chessboard[r][c].draw(canvas, type, getContext());

            }
        }
    }

    private int getXCoord(int x) {
        return x0 + squareSize * x;
    }

    private int getYCoord(int y) {
        return y0 + squareSize * y;
    }

    public void straightmove(String origin, String dest) {
        int[] o = this.getCoord(origin);
        int[] d = this.getCoord(dest);

        this.promote = this.moves.get(movecount).getPromote();
        if (this.getchessboard()[o[0]][o[1]].empty) {
            return;
        }
        Piece p = this.getchessboard()[o[0]][o[1]].getPiece();

        //en passant for white
        if(p.type == 'P' && p.color == 'w' && o[0] == 3 && d[1] != o[1] && this.getchessboard()[d[0]][d[1]].empty){
            this.getchessboard()[d[0]][d[1]].setPiece(p);
            chessboard[d[0]][d[1]].getPiece().setRow(d[0]);
            chessboard[d[0]][d[1]].getPiece().setCol(d[1]);
            this.getchessboard()[o[0]][o[1]].rem();
            this.getchessboard()[o[0]][d[1]].rem();
        }

       else  if(p.type == 'P' && p.color == 'b' && o[0] == 4 && d[1] != o[1] && this.getchessboard()[d[0]][d[1]].empty){
            this.getchessboard()[d[0]][d[1]].setPiece(p);
            chessboard[d[0]][d[1]].getPiece().setRow(d[0]);
            chessboard[d[0]][d[1]].getPiece().setCol(d[1]);
            this.getchessboard()[o[0]][o[1]].rem();
            this.getchessboard()[o[0]][d[1]].rem();
        }


       else if (p.type == 'P' && (d[0] == 0 || d[0] == 7)) {
            //System.out.println("trying to promote");


            if (this.movecount % 2 == 0) {
                switch (promote) {
                    case 'X':
                        this.getchessboard()[d[0]][d[1]].setPiece(new Queen(d[0], d[1], 'Q', 'w'));
                        this.getchessboard()[o[0]][o[1]].rem();
                        break;

                    case 'Q':
                        this.getchessboard()[d[0]][d[1]].setPiece(new Queen(d[0], d[1], 'Q', 'w'));
                        this.getchessboard()[o[0]][o[1]].rem();
                        break;

                    case 'N':
                        this.getchessboard()[d[0]][d[1]].setPiece(new Knight(d[0], d[1], 'N', 'w'));
                        this.getchessboard()[o[0]][o[1]].rem();
                        break;

                    case 'R':
                        this.getchessboard()[d[0]][d[1]].setPiece(new Rook(d[0], d[1], 'R', 'w'));
                        this.getchessboard()[o[0]][o[1]].rem();
                        break;

                    case 'B':
                        this.getchessboard()[d[0]][d[1]].setPiece(new Bishop(d[0], d[1], 'B', 'w'));
                        this.getchessboard()[o[0]][o[1]].rem();
                        break;
                }

            }

            if (this.movecount % 2 == 1) {
                switch (promote) {
                    case 'Q':
                        this.getchessboard()[d[0]][d[1]].setPiece(new Queen(d[0], d[1], 'Q', 'b'));
                        this.getchessboard()[o[0]][o[1]].rem();
                        break;

                    case 'N':
                        this.getchessboard()[d[0]][d[1]].setPiece(new Knight(d[0], d[1], 'N', 'b'));
                        this.getchessboard()[o[0]][o[1]].rem();
                        break;

                    case 'R':
                        this.getchessboard()[d[0]][d[1]].setPiece(new Rook(d[0], d[1], 'R', 'b'));
                        this.getchessboard()[o[0]][o[1]].rem();
                        break;

                    case 'B':
                        this.getchessboard()[d[0]][d[1]].setPiece(new Bishop(d[0], d[1], 'B', 'b'));
                        this.getchessboard()[o[0]][o[1]].rem();
                        break;
                }

            }


        } else {

            this.getchessboard()[d[0]][d[1]].setPiece(p);
            chessboard[d[0]][d[1]].getPiece().setRow(d[0]);
            chessboard[d[0]][d[1]].getPiece().setCol(d[1]);
            this.getchessboard()[o[0]][o[1]].rem();
            //this.origin.Selected1 = true;
            //this.origin = null;
        }
    }

    public void move(String origin, String dest, Chessboard_replay b) {
        int[] o = b.getCoord(origin);
        int[] d = b.getCoord(dest);

            Piece p = b.getchessboard()[o[0]][o[1]].getPiece();
            b.getchessboard()[d[0]][d[1]].setPiece(p);
            b.getchessboard()[d[0]][d[1]].getPiece().setRow(d[0]);
            b.getchessboard()[d[0]][d[1]].getPiece().setCol(d[1]);
            b.getchessboard()[o[0]][o[1]].rem();

    }


}
