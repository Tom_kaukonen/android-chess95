package com.tkauk.chessapp;

/**
 * Created by tkauk on 4/20/2017.
 */

public class Queen extends Piece{

    public Queen(String coord, char type, char color) {
        super(coord, type, color);
        // TODO Auto-generated constructor stub
    }

    public Queen(int row, int col, char type, char color){
        super(row,col,type,color);
    }


    @Override
    public String toString(){
        if(color == 'w'){
            return "wQ";
        }
        return "bQ";

    }

    /**
     * checks if the queen can move to a square
     * @param origin
     * @param dest
     * @param b
     * @return boolean indicating if the queen can move to a given square
     */
    @Override
    public boolean validmovement(String origin, String dest, ChessBoard b){


/*
        int[] o = new int[2];
        int[] d = new int[2];

        o[0] = b.origin.getRow();
        o[1] = b.origin.getCol();

        d[0] = b.dest.getRow();
        d[1] = b.dest.getCol();
        */
        int[] o = b.getCoord(origin);
        int[] d = b.getCoord(dest);
        int originrow = o[0];
        int origincol = o[1];
        int destrow = d[0];
        int destcol = d[1];
        if(b.getSquare(destrow,destcol).getPiece()!=null){
            if(b.getSquare(originrow,origincol).getPiece().getColor()==b.getSquare(destrow,destcol).getPiece().getColor()){
                return false;
            }
        }

//		boolean noobstacle = true;//true if there is no obstacle between origin and dest
        if(originrow==destrow){
            boolean noobstacle = true;
            if(origincol<destcol){
                for(int i=1;i<Math.abs(origincol-destcol);i++){
                    if(b.getSquare(originrow, origincol+i).getPiece() != null){
                        noobstacle=false;
                    }
                }
            }else if(origincol>destcol){
                for(int i=1;i<Math.abs(origincol-destcol);i++){
                    if(b.getSquare(originrow, origincol-i).getPiece() != null){
                        noobstacle=false;
                    }
                }
            }
            return noobstacle;
        }else if(origincol == destcol){
            boolean noobstacle = true;
            if(originrow<destrow){
                for(int i=1;i<Math.abs(originrow-destrow);i++){
                    if(b.getSquare(originrow+i, origincol).getPiece() != null){
                        noobstacle=false;
                    }
                }
            }else if(originrow>destrow){
                for(int i=1;i<Math.abs(originrow-destrow);i++){
                    if(b.getSquare(originrow-i, origincol).getPiece() != null){
                        noobstacle=false;
                    }
                }
            }
            return noobstacle;
        }else if(Math.abs(originrow-destrow)==Math.abs(origincol-destcol)){
            boolean noobstacle = true;
            if(originrow<destrow && origincol<destcol){
                for(int i=1;i<Math.abs(originrow-destrow);i++){
                    if(b.getSquare(originrow+i, origincol+i).getPiece() != null){
                        noobstacle=false;
                    }
                }
            }else if(originrow<destrow && origincol>destcol){
                for(int i=1;i<Math.abs(originrow-destrow);i++){
                    if(b.getSquare(originrow+i, origincol-i).getPiece() != null){
                        noobstacle=false;
                    }
                }
            }else if(originrow>destrow && origincol<destcol){
                for(int i=1;i<Math.abs(originrow-destrow);i++){
                    if(b.getSquare(originrow-i, origincol+i).getPiece() != null){
                        noobstacle=false;
                    }
                }
            }else if(originrow>destrow && origincol>destcol){
                for(int i=1;i<Math.abs(originrow-destrow);i++){
                    if(b.getSquare(originrow-i, origincol-i).getPiece() != null){
                        noobstacle=false;
                    }
                }
            }
            return noobstacle;
        }
        return false;
    }
}
